import { sign, verify } from 'jsonwebtoken'

import { ACCESS_SECRET, ACCESS_EXPIRES } from 'config'

export const generateJWTToken = id => 
  sign({ id: id }, ACCESS_SECRET, { expiresIn: ACCESS_EXPIRES })

export const verifyJWTToken = token => verify(token, ACCESS_SECRET)
