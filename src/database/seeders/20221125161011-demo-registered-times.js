'use strict'

export default {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Registered_times',
      [
        {
          id: 'ad6360fb-67d8-4232-b40a-dd36207f4efb',
          user_id: '4a26f6b7-25bd-4340-bb41-092a059733bc',
          time_registered: '2022-11-27T16:10:00'
        },
        {
          id: '7b3db1b7-4eef-465d-b298-12616d1895a4',
          user_id: '8dc33964-83e5-4ee4-85c8-d38d8c0b7df2',
          time_registered: '2022-11-27T15:09:00'
        },
        {
          id: '8916178a-a6f6-4ee2-b0dc-78d67c345061',
          user_id: '4a26f6b7-25bd-4340-bb41-092a059733bc',
          time_registered: '2022-11-27T15:07:00'
        },
        {
          id: '0057fb8c-ae94-4e5b-97dc-bc4aa4ae05b7',
          user_id: '8dc33964-83e5-4ee4-85c8-d38d8c0b7df2',
          time_registered: '2022-11-27T11:09:00'
        },
        {
          id: '96985fe4-4525-4de9-a55c-7a40aff9412c',
          user_id: '4a26f6b7-25bd-4340-bb41-092a059733bc',
          time_registered: '2022-11-27T11:07:00'
        },
        {
          id: 'c2b54626-079e-41b8-bd52-f23805f59007',
          user_id: '4a26f6b7-25bd-4340-bb41-092a059733bc',
          time_registered: '2022-11-26T20:10:00'
        },
        {
          id: '020db39d-ec3a-4eea-8ca2-bfc7536556a2',
          user_id: '8dc33964-83e5-4ee4-85c8-d38d8c0b7df2',
          time_registered: '2022-11-26T16:00:00'
        },
        {
          id: 'cbc3758a-62f5-4041-9273-9f52fa637f09',
          user_id: '8dc33964-83e5-4ee4-85c8-d38d8c0b7df2',
          time_registered: '2022-11-26T15:03:00'
        },
        {
          id: '849bd024-c5eb-4760-8170-cf8e82b9c78f',
          user_id: '8dc33964-83e5-4ee4-85c8-d38d8c0b7df2',
          time_registered: '2022-11-26T11:01:00'
        }
      ],
      {}
    )
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Registered_times', null, {})
  }
}
