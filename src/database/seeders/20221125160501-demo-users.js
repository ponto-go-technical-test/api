'use strict'
import bcrypt from 'bcrypt'

export default {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Users',
      [
        {
          id: '4a26f6b7-25bd-4340-bb41-092a059733bc',
          name: 'Ciclano',
          email: 'ciclano@dominio.com',
          password: await bcrypt.hash('12345678', 12),
          role: 'led',
          employee_code: '003'
        },
        {
          id: '8dc33964-83e5-4ee4-85c8-d38d8c0b7df2',
          name: 'Beltrana',
          email: 'beltrana@dominio.com',
          password: await bcrypt.hash('12345678', 12),
          role: 'leader',
          employee_code: '002'
        },
        {
          id: 'f4eaf37c-67ce-43ab-bd77-3d5a81435e59',
          name: 'Fulano',
          email: 'fulano@dominio.com',
          password: await bcrypt.hash('12345678', 12),
          role: 'administrator',
          employee_code: '001'
        }
      ],
      {}
    )
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {})
  }
}
