'use strict'
export default {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false
      },
      name: {
        type: Sequelize.STRING(45)
      },
      email: {
        type: Sequelize.STRING(45),
        unique: true
      },
      password: {
        type: Sequelize.STRING(60)
      },
      passwordResetToken: {
        type: Sequelize.STRING(8),
        unique: true
      },
      role: {
        type: Sequelize.STRING(45)
      },
      employee_code: {
        unique: true,
        allowNull: false,
        type: Sequelize.INTEGER(3),
      }, 
      createdAt: {
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users')
  }
}
