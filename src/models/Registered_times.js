'use strict'
import { Model } from 'sequelize'

export default (sequelize, DataTypes) => {
  class Registered_times extends Model {
    static associate(models) {
      Registered_times.belongsTo(models.Users, { foreignKey: 'user_id' })
    }
  }
  Registered_times.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      user_id: {
        type: DataTypes.UUID
      },
      time_registered: {
        type: DataTypes.DATE,
        validate: {
          notEmpty: { msg: 'Insira a data e a hora do ponto' }
        }
      }
    },
    {
      sequelize,
      modelName: 'Registered_times'
    }
  )
  return Registered_times
}
