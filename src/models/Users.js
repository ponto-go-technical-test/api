'use strict'
import { Model } from 'sequelize'

export default (sequelize, DataTypes) => {
  class Users extends Model {
    static associate(models) {
      Users.hasMany(models.Registered_times, { foreignKey: 'user_id' })
    }
  }
  Users.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      name: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Insira o nome' },
          len: { args: [3, 45], msg: 'O nome deve conter no mínimo 3 caracters' }
        }
      },
      email: {
        type: DataTypes.STRING,
        unique: { msg: 'O email deve ser único' },
        validate: {
          isEmail: { msg: 'Email inválido' },
          notEmpty: { msg: 'Insira o email' }
        }
      },
      password: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Insira a senha' },
          len: {
            args: [8, 60],
            msg: 'A senha deve conter no mínimo 8 caracters'
          }
        }
      },
      passwordResetToken: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [8, 8],
            msg: 'O token possui 8 caracters'
          }
        }
      },
      role: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Insira a função' },
          len: {
            args: [3, 45],
            msg: 'A função deve conter no mínimo 3 caracters'
          }
        }
      },
      employee_code: {
        type: DataTypes.INTEGER(3),
        unique: { msg: 'O código de funcionário deve ser único' }
      }
    },
    {
      sequelize,
      modelName: 'Users'
    }
  )
  return Users
}
