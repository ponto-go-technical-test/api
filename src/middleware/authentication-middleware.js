import db from 'models'
import { verifyJWTToken, Unauthorized } from 'helpers'

const Users = db.Users

export const authenticationMiddleware = async (req, res, next) => {
  const { authorization } = req.headers

  if (!authorization) throw new Unauthorized('Token não informado')

  const [type, token] = authorization.split(' ')

  const { id } = verifyJWTToken(token)

  const { email, role, ...user } = await Users.findByPk(id)
  if (!user) throw new Unauthorized('Acesso não autorizado')

  const loggedUser = { id: id, email: email, role: role }
  req.user = loggedUser

  next()
}
