import db from 'models'
import { ADMINISTRATOR, Unauthorized } from 'helpers'

const Registered_times = db.Registered_times
const Users = db.Users

export const storeRegisteredTime = async (req, res) => {
  const { id: loggedUserId } = req.user
  const { id: parameterUserId } = req.params
  const { time_registered } = req.body

  const loggedUser = await Users.findByPk(loggedUserId)

  if (loggedUserId !== parameterUserId || loggedUser.role === ADMINISTRATOR)
    throw new Unauthorized('Criação de registro não autorizada')

  const registered_time = await Registered_times.create({
    user_id: loggedUserId,
    time_registered: time_registered
  })

  const data = {
    id: registered_time.id,
    user_id: registered_time.user_id,
    time_registered: registered_time.time_registered
  }

  return res.status(201).json(data)
}

export const showRegisteredTimeByUser = async (req, res) => {
  const { id: loggedUserId } = req.user
  const { id: parameterUserId } = req.params

  const loggedUser = await Users.findByPk(loggedUserId)

  if (loggedUserId === parameterUserId || loggedUser.role === ADMINISTRATOR) {
    const registered_times = await Registered_times.findAll({
      where: { user_id: parameterUserId },
      order: [['time_registered', 'DESC']],
      attributes: ['id', 'user_id', 'time_registered'],
      include: [{
        model: Users,
        attributes: ['name', 'employee_code']
      }]
    })

    return res.status(200).json(registered_times)
  } else {
    throw new Error('Acesso a registros não autorizado')
  }
}

export default {
  storeRegisteredTime,
  showRegisteredTimeByUser
}
