import bcrypt from 'bcrypt'
import crypto from 'crypto'

import db from 'models'
import { 
  generateJWTToken, 
  ADMINISTRATOR, 
  Unauthorized, 
  NotFound, 
  sendEmail, 
  encryptPassword
} from 'helpers'
import { templateForgetPassword } from 'utils'

const Users = db.Users

export const showUser = async (req, res) => {
  const { id: loggedUserId } = req.user
  const { id: parameterUserId } = req.params

  const loggedUser = await Users.findByPk(loggedUserId)
  
  if (loggedUserId === parameterUserId || loggedUser.role === ADMINISTRATOR) {
    
    const user = await Users.findByPk(parameterUserId, {
      attributes: ['id', 'name', 'email', 'role']
    })
    if (!user) throw new NotFound('Usuário não encontrado')
  
    return res.status(200).json(user)
  
  } else {
    throw new Unauthorized('Acesso a usuário não autorizado')
  }
}

export const updateUser = async (req, res) => {
  const { id: loggedUserId } = req.user
  const { id: parameterUserId } = req.params
  const { name, email, role } = req.body

  if (loggedUserId !== parameterUserId) 
    throw new Unauthorized('Atualização de usuário não autorizada')

  const user = await Users.findByPk(parameterUserId)
  if (!user) throw new NotFound('Usuário não encontrado')

  await user.update({ name, email, role })

  return res.status(204).end()
}

export const login = async (req, res) => {
  const { email, password } = req.body

  const user = await Users.findOne({ where: { email } })

  if (!user || !(await bcrypt.compare(password, user.password)))
    throw new Unauthorized('Email e/ou senha inválido(s)')

  const data = {
    id: user.id,
    email: user.email,
    role: user.role,
    accessToken: generateJWTToken(user.id)
  }

  return res.status(200).json(data)
}

export const me = (req, res) => res.json(req.user)

export const forgetPassword = async (req, res) => {
  const { email } = req.body

  const user = await Users.findOne({ where: { email } })
  if (!user) throw new NotFound('Email inválido(s)')

  await user.update({ passwordResetToken: crypto.randomBytes(4).toString('HEX') })

  const template = templateForgetPassword(user.passwordResetToken)
  await sendEmail(email, template)

  return res.status(204).end()
}

export const redefinePassword = async (req, res) => {
  const { token, password } = req.body

  const user = await Users.findOne({ where: { passwordResetToken: token } })
  if (!user) throw new Unauthorized('Código de verificação inválido')

  await user.update({
    password: await encryptPassword(password),
    passwordResetToken: null
  })

  return res.status(200).end()
}

export default {
  showUser,
  updateUser,
  login,
  me,
  forgetPassword,
  redefinePassword
}
