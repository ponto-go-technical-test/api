import db from 'models'
import { encryptPassword, ADMINISTRATOR, Unauthorized, NotFound } from 'helpers'

const Registered_times = db.Registered_times
const Users = db.Users


export const storeUser = async (req, res) => {
  const { id: loggedUserId } = req.user
  const { name, email, password, role, employee_code } = req.body

  const loggedUser = await Users.findByPk(loggedUserId)
  if (loggedUser.role !== ADMINISTRATOR) 
    throw new Unauthorized('Criação de usuário não autorizada')

  const user = await Users.findOne({ where: { email } })
  if (user) throw new NotFound('E-mail fornecido já cadastrado')

  const newUser = await Users.create({
    name,
    email,
    password: await encryptPassword(password),
    role,
    employee_code
  })

  const data = {
    id: newUser.id,
    name: newUser.name,
    email: newUser.email,
    role: newUser.role,
    employee_code: newUser.employee_code
  }

  return res.status(201).json(data)
}

export const indexUser = async (req, res) => {
  const { id: loggedUserId } = req.user

  const loggedUser = await Users.findByPk(loggedUserId)
  if (loggedUser.role !== ADMINISTRATOR)
    throw new Unauthorized('Acesso a lista de usuários não autorizado')

  const users = await Users.findAll({
    attributes: ['id', 'name', 'email', 'role']
  })

  return res.status(200).json(users)
}

export const destroyUser = async (req, res) => {
  const { id: loggedUserId } = req.user
  const { id: parameterUserId } = req.params

  const loggedUser = await Users.findByPk(loggedUserId)
  if (loggedUser.role !== ADMINISTRATOR)
    throw new Unauthorized('Exclusão de usuário não autorizada')

  const user = await Users.findByPk(parameterUserId)
  if (!user) throw new NotFound('Usuário não encontrado')

  await user.destroy({ where: { id: user.id } })

  return res.status(204).end()
}

export const showRegisteredTimeAllUsers = async (req, res) => {
  const { id: loggedUserId } = req.user

  const loggedUser = await Users.findByPk(loggedUserId)

  if (loggedUser.role !== ADMINISTRATOR) 
    throw new Unauthorized('Acesso a registros não autorizado')

  const registered_times = await Registered_times.findAll({
    order: [['time_registered', 'DESC']],
    attributes: ['id', 'user_id', 'time_registered'],
    include: [{
      model: Users,
      attributes: ['name', 'employee_code']
    }]
  })
  return res.status(200).json(registered_times)
}

export default {
  showRegisteredTimeAllUsers,
  storeUser,
  indexUser,
  destroyUser
}
