import { Router } from 'express'

import users from './users-router'
import administrator from './administrator-routes'
import registeredTimes from './registered-times-router'

const router = Router()

router.use(users)
router.use(administrator)
router.use(registeredTimes)

export default router
