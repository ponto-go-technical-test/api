import { Router } from 'express'

import { authenticationMiddleware } from 'middleware'
import administratorController from 'controllers/administrator-controller'

const routes = Router()

routes.post('/admin/users/signup', authenticationMiddleware, administratorController.storeUser)
routes.get('/admin/users', authenticationMiddleware, administratorController.indexUser)
routes.delete('/admin/users/:id', authenticationMiddleware, administratorController.destroyUser)
routes.get(
  '/admin/users/registered-times',
  authenticationMiddleware,
  administratorController.showRegisteredTimeAllUsers
)

export default routes
