import { Router } from 'express'

import { authenticationMiddleware } from 'middleware'
import registeredTimesController from 'controllers/registered-times-controller'

const routes = Router()

routes.post(
  '/users/:id/registered-times',
  authenticationMiddleware,
  registeredTimesController.storeRegisteredTime
)

routes.get(
  '/users/:id/registered-times',
  authenticationMiddleware,
  registeredTimesController.showRegisteredTimeByUser
)

export default routes
