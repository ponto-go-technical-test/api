import { Router } from 'express'

import { authenticationMiddleware } from 'middleware'
import usersController from 'controllers/users-controller'

const routes = Router()

routes.get('/users/:id', authenticationMiddleware, usersController.showUser)
routes.put('/users/:id', authenticationMiddleware, usersController.updateUser)

routes.get('/me', authenticationMiddleware, usersController.me)
routes.post('/users/login', usersController.login)
routes.post('/users/forget-password', usersController.forgetPassword)
routes.post('/users/redefine-password', usersController.redefinePassword)

export default routes