export const templateForgetPassword = token => {
  return {
    subject: 'Recuperação de senha',
    text: `Acesse o link /users/reset e fornece o código: ${token} para redefinir sua senha.`,
    html: `
      <p>Prezado(a), você solicitou a redefinição de senha.</p> <br />
      <p>Acesse a rota /users/redefine-password e fornece o código: ${token} para redefinir sua senha.</p>
    `
  }
}
