import 'express-async-errors'
import express from 'express'
import cors from 'cors'

import router from 'routes'
import { errorHandlingMiddleware } from './middleware'

const app = express()

app.use(
  cors({
    origin: '*',
    allowMethods: ['GET', 'HEAD', 'PUT', 'POST', 'DELETE', 'PATCH'],
    exposeHeaders: ['X-Request-Id']
  })
)

app.use(express.json())

app.use(router)
app.use(errorHandlingMiddleware)

export default app
