# Ponto Go API

## Tecnologias utilizadas
RESTful usando Node.js, Express, Mysql e Sequelize.

# Postman
 A documentação da API pode ser utilizada com o [**Postman**](https://documenter.getpostman.com/view/16658273/2s8YsryE1J). 

**_Coleção disponível na raiz do projeto_**

## Arquitetura
Para a criação do projeto foi utilizado o padrão arquitetural de software MVC.

## Siga os passos abaixo para executar o projeto

1) Crie um banco de dados

2) Renomeie o arquivo da raiz do projeto chamado .env.example para .env e neste defina:

> - A porta para o servidor rodar**
> - Seus dados de acesso ao banco de dados**
> - A chave secreta para o token JWT
> - O tempo de expiração do token JWT

** **_Obrigatório_**


3) Inicie as dependências do projeto
```
 yarn install
```

4) Execute as migrações para criação das tabelas no banco de dados
```
yarn run db:migrate
```

5) Execute as seeds para população das tabelas no banco de dados
```
yarn run db:seed
```

6) Realize o login com um dos seguintes usuários

Efetue o login na rota **/users/login** para receber o token JWT. 
Adicione este token ao cabeçalho authorization de suas solicitações seguintes para obter acesso as demais rotas do sistema.

**Administrator**
> ```
> email: fulano@dominio.com
> password: 12345678
> ```

**Led**
> ```
> email: ciclano@dominio.com
> password: 12345678
> ```


7) Inicie o servidor
```
yarn run dev
``` 

**_Vide seção scripts em package.json na raiz do projeto para conhecimento de comandos auxiliares._**

## Estrutura de diretórios

```
├── /src
|   ├── /controllers
|   ├── /database
|   |   ├── /config
|   |   ├── /migrations
|   |   ├── /seeders
|   ├── /helpers
|   ├── /middlewares
|   ├── /models
|   ├── /routes
|   ├── /utils
```

## Modelo relacional do banco de dados

![ponto_go](https://user-images.githubusercontent.com/63760217/204540625-e1e9a629-b7d8-443f-9a30-d1c6e5d06e60.png)